package com.geek.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.geek.model.ReadExcelFileToList;

public class MyResultServlet extends HttpServlet {
	String hallticket;
	String email;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html;charset=UTF-8");
	    PrintWriter out = resp.getWriter();
	    
	        // get access to file that is uploaded from client
	    String filepath = req.getParameter("file");
	    String actualPath = "D:/"+filepath;
	    System.out.println(actualPath);
	    ReadExcelFileToList.readExcelData(actualPath);
	    
	       
	}
	    
}
