package com.geek.pojo;

import javax.persistence.Entity;

@Entity
public class Student {
	private int id;
	private String name;
	private String email;
	private String hallticket;
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getHallticket() {
		return hallticket;
	}
	public void setHallticket(String hallticket) {
		this.hallticket = hallticket;
	}

}
