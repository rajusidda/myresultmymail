package com.geek.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFileToList {
	static Cell cell;
	String string;
	static String listOfCells[];

	public static List<String> readExcelData(String fileName) {
		List<String> List = new ArrayList<String>();

		try {
			// Create the input stream from the xlsx/xls file
			FileInputStream fis = new FileInputStream(fileName);

			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}

			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();

			// loop through each of the sheets
			for (int i = 0; i < numberOfSheets; i++) {

				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);

				// every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) {
					String name = "";
					String shortCode = "";

					// Get the row object
					Row row = rowIterator.next();

					// Every row has columns, get the column iterator and
					// iterate over them
					Iterator<Cell> cellIterator = row.cellIterator();

					while (cellIterator.hasNext()) {
						// Get the Cell object
						cell = cellIterator.next();
						
						// data base connection 
						Statement st;
						ResultSet rs;
						Connection conn;
						String hallticketnum = null;
						HSSFCell cell2 ;
						 String cell1 = null;
						 String hallnum = null;
						 String hallcheck =null;
						 String cel2 = null;
						try {
							Class.forName("com.mysql.jdbc.Driver");
							conn = DriverManager.getConnection(
									"jdbc:mysql://localhost:3306/result",
									"root", "root");
							System.out.println("connected at line 27");
							st = conn.createStatement();
							String sql = ("SELECT * FROM student ");
							rs = st.executeQuery(sql);

							while (rs.next()) {
								/*
								 * String id = rs.getString("name");
								 * System.out.println(id);
								 */
								// getting halltickets  from data base 
								hallticketnum = rs.getString("hallticket");
								
								//getting values from xl sheet and convert into string
								
								String hallticketcell = cell.toString();
								String halltick = "hallticket";
								
								//compare upcoming valuefrom db is hall ticket or not, 
								//if hall ticket compare hall tickets both exist in Db and xl sheet   
								
								if(hallticketcell.equals(halltick)){
									for (int j=0; j< sheet.getLastRowNum() + 1; j++) {
								        Row row5 = sheet.getRow(j);
								        Cell celll = row5.getCell(0); 
								        hallnum = celll.toString();
								        System.out.println(celll);
								        
								        if(hallticketnum != null && hallnum!=null){
								        
								       // compare two hall tickets (DB and Xl)
								        	
								        if(hallnum.equals(hallticketnum)) {
								        	hallcheck=hallticketnum;
											System.out.println("matched");
											if(row != null){
												
										// if both hall tickets matched get the row (ex: hall ticket, sub1, sub2, total marks, secured marks etc.. 
												
											Iterator<Cell> cells = row.cellIterator();
											while (cells.hasNext()) {
									            cell2 = (HSSFCell) cells.next();
									            cell1 = cell2.toString();
									            System.out.println(cell1);
											}
											
										//Here i am trying to get the values of matched hall ticked row (ex: 08au1a0582,200,200,600,400 etc
		//------------------------------//this is pending work and also merge mailing code (we have mail class also).	
											
											Iterator<Cell> cellIterator1 = row.cellIterator();

											while (cellIterator1.hasNext()) {
												// Get the Cell object
												 Cell next = cellIterator1.next();
											cel2 = next.toString();
										
											if(cel2.equals(hallcheck)){
												Iterator<Cell> celle = row.cellIterator();
												while (celle.hasNext()) {
										            HSSFCell cellr = (HSSFCell) celle.next();
										            String cell4 = cellr.toString();
										            System.out.println("this for "+ "---------------"+""+cell4);
											}
											}
											}
											
											}
											
											int row2 = cell.getColumnIndex();
											Row row3 = cell.getRow();
											
											row3.getCell(row2);
											System.out.println("getRow" + row3.getCell(row2));
											
											System.out.println("getColumnIndex-" +row2);
											
										}
								        }else {
											int row2 = cell.getColumnIndex();
											Row row3 = cell.getRow();
											
											row3.getCell(row2);
											System.out.println("getRow" + row3.getCell(row2));
											
											System.out.println("getColumnIndex-" +row2);
										}
								    }
								}
								
							}
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						


						// check the cell type and process accordingly
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
							if (shortCode.equalsIgnoreCase("")) {
								shortCode = cell.getStringCellValue().trim();
							} else if (name.equalsIgnoreCase("")) {
								// 2nd column
								name = cell.getStringCellValue().trim();
							} else {
								// random data, leave it
								/*
								 * System.out.println("Random data::" +
								 * cell.getStringCellValue());
								 */
							}
							break;
						case Cell.CELL_TYPE_NUMERIC:
							/*
							 * System.out.println("Random data::" +
							 * cell.getNumericCellValue());
							 */
						}
					} // end of cell iterator

				} // end of rows iterator

			} // end of sheets for loop

			// close file input stream
			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * DBConnection connection = new DBConnection();
		 * connection.checkValues();
		 */
		return List;
	}

}