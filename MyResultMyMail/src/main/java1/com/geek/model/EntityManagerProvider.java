package com.geek.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerProvider {


	private static EntityManagerFactory entityManagerFactory = null;

	private EntityManagerProvider(){
	}
	public static EntityManager getEntityManager() {
		entityManagerFactory = Persistence.createEntityManagerFactory("result", getMap());
		EntityManager entityManager = null;
		if (entityManagerFactory != null) {
			entityManager = entityManagerFactory.createEntityManager();
		}
		return entityManager;
	}

	private static Map<String, String> getMap() {
		HashMap<String, String> CofigOverrides = new HashMap<String, String>();
		CofigOverrides.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		CofigOverrides.put("javax.persistence.jdbc.user", "root");
		CofigOverrides.put("javax.persistence.jdbc.password", "root");
		CofigOverrides.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/result");
		return CofigOverrides;
	}
	

}